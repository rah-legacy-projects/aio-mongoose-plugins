module.exports = exports = function(schema,options) {
	schema.add({created: Date });
	schema.add({modified: Date});
	schema.pre('save',function(next) {
		if(this.created == null) { this.created = new Date(); }
		this.modified = new Date();
		if(next) { next(); }
	});
	if(options && options.index) {
		schema.path('created').index(options.index);
		schema.path('modified').index(options.index);
	}
};
