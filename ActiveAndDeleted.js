module.exports = exports = function(schema,options) {
	schema.add({active:Boolean, deleted:Boolean});
	schema.path('active').required(true).default(true);
	schema.path('deleted').required(true).default(false);
};