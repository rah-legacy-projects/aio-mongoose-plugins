module.exports = exports = function(schema,options) {
	schema.add({text:String, description:String});
	if(options && options.required) {
		schema.path('text').required(true);
	}
	if(options && options.index) {
		schema.path('text').index(options.index);
	}
};