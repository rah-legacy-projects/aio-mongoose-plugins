var mongoose = require('mongoose')
	, Schema = mongoose.Schema
    , ObjectId = Schema.Types.ObjectId;

function Plugins() {
	this.options = {};
	this.Plugins = Plugins;
	this.AclObject = require('./AclObject');
	this.AclSubject = require('./AclSubject');
	this.CreatedAndModified = require('./CreatedAndModified');
	this.TextItem = require('./TextItem');
	this.ActiveAndDeleted = require('./ActiveAndDeleted')
};

var plugins = module.exports = exports = new Plugins();